package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MyMenuBar {
	MenuBar bar = null;
	String person = new String();
	Sheet sheetdata;

	MyMenuBar(Sheet sheetdata, Stage pstage, PostgresLoginInfo logininfo, MyPMTGUI sheetgui){
		this.sheetdata = sheetdata;
		bar = new MenuBar();
		Menu m1 = new Menu("メニュー");
		MenuItem menuNew = new MenuItem("新規作成");
		MenuItem menuSave = new MenuItem("ローカルに保存");
		MenuItem menuLoad = new MenuItem("ローカルから読み込み");
		MenuItem menuSQLSave = new MenuItem("サーバーにアップ");
		MenuItem menuSQLLoad = new MenuItem("サーバーからロード");
		MenuItem menuExit = new MenuItem("終了");

		m1.getItems().addAll(menuNew, menuSave, menuLoad, menuSQLSave, menuSQLLoad, menuExit);
		bar.getMenus().add(m1);

		final FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File("./"));
		fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("バイナリファイル", "*.bin") );
		fileChooser.setInitialFileName("ProjectManage.bin");

		menuNew.setOnAction((event) -> {
			Sheet newsheet = new Sheet();
			newsheet.setTestHead();
			newsheet.insert_new_task();
			pstage.close();
			MainWindow mw = new MainWindow(newsheet, new Stage(), logininfo);
		});

		// 保存選択時
		menuSave.setOnAction((event)-> {
			sheetgui.writetoObject();
			File choosedFile = fileChooser.showSaveDialog(pstage);
			String filename = choosedFile.getAbsolutePath();
			// sheet 保存
            save_sheet(sheetdata);
			try (FileOutputStream file = new FileOutputStream(filename);
	                ObjectOutputStream stream = new ObjectOutputStream(file);){
	            stream.writeObject(sheetdata);
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		});

		// 読み込み選択時
		menuLoad.setOnAction((event) -> {
			File choosedFile = fileChooser.showOpenDialog(pstage);
			String filename = choosedFile.getAbsolutePath();

			pstage.close();
			// sheet 読み込み

			Sheet loaded = null;

	        try (FileInputStream fileInputStream = new FileInputStream(filename);
	                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);){

	            loaded = (Sheet)objectInputStream.readObject();

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }

			Stage stage = new Stage();
			if(loaded == null) {

			} else {
				MainWindow mw = new MainWindow(loaded, stage, logininfo);
			}
		});

		menuSQLSave.setOnAction((event) -> {
			sheetgui.writetoObject();
			try {
				new JDBC(sheetdata, logininfo).saveToSheet();;
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				System.out.println("SQLエラー！");
				e.printStackTrace();
			}
		});

		menuSQLLoad.setOnAction((event) -> {
			pstage.close();
			Sheet loaded2 = null;
			try {
				loaded2 = new JDBC(sheetdata, logininfo).loadFromSheet();
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			Stage stage = new Stage();
			if(loaded2 == null) {

			} else {
				MainWindow mw = new MainWindow(loaded2, stage, logininfo);
			}
		});

		menuExit.setOnAction((event)-> {
			System.exit(0);
		});
	}

	MenuBar getMenuBar() {
		return bar;
	}

	Sheet getSheet() {
		return sheetdata;
	}

	void loadFromServer() {

	}

	String editlabel(Label label) {
		// 新しいウインドウを生成
		Stage newStage = new Stage();
		// モーダルウインドウに設定
		newStage.initModality(Modality.APPLICATION_MODAL);
		// オーナーを設定
//		newStage.initOwner(primaryStage);

		// 新しいウインドウ内に配置するコンテンツを生成
		GridPane gp = new GridPane();
		gp.setAlignment(Pos.CENTER);
		gp.setHgap(10);
		gp.setVgap(10);
		gp.setPadding(new Insets(25, 25, 25, 25));

        Label l = LabelBuilder.create().text("そのようなファイルはありません")
                .prefWidth(800)
                .prefHeight(40)
                .alignment(Pos.CENTER)
                .build();
		gp.add(l, 0, 0, 2, 1);



		Scene scene = new Scene(gp, 300, 275);
		scene.getStylesheets().add(getClass().getResource("application.css").toString());
		gp.getStyleClass().addAll("editwindow");

		newStage.setScene(scene);
		// 新しいウインドウを表示
		newStage.show();

		return label.getText();

	}

	static void save_sheet(Sheet sheet) {

		List<Task> tasklist = sheet.get_tasklist();

		/*
		 * シートからタスクリストを取得し，それぞれ表示
		 */

		// 記入者と記入日について
		System.out.println("記入者：" + sheet.get_author().grade + " " + sheet.get_author().get_name());
		System.out.println("記入日：" + sheet.get_filleddate());

		// タスクを１つずつ取り出し内容を表示
        int i=0;
		for(Task task : tasklist) {
			System.out.println("--------- "+ (++i) +"番目のタスク --------");

			System.out.println("実施内容：" + task.taskdetail.get_taskname());
			System.out.println("実施者：" + task.taskdetail.get_personandplace());
			System.out.println("実施時期：" + "なうこーでぃんぐ");
			System.out.println("完成度：" + task.perfection.get_perfection());
			System.out.println("問題点・備考：" + task.devcomment.get_comment());
			System.out.println("チュータ意見：" + task.tutorcomment.get_comment());
		}


	}


}
