package application;

import java.io.Serializable;

public class TaskDetail implements Serializable{
	String taskname;
	String personandplace;

	void set_taskname(String taskname) {
		this.taskname = taskname;
	}
	void set_personandplace(String personandplace) {
		this.personandplace = personandplace;
	}
	String get_taskname() {
		return taskname;
	}
	String get_personandplace() {
		return personandplace;
	}

}
