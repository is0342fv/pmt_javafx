package application;

import java.io.Serializable;

public class Person implements Serializable{
	int id;
	String name;
	String grade;
	String semi;

	Person() {

	}

	Person(String name) {
		this.name = name;
	}

	/*
	 * 入力引数の検査の必要性？
	 */
	void set_id(int id){
		this.id = id;
	}
	void set_name(String name){
		this.name = name;
	}
	void set_grade(String grade){
		this.grade = grade;
	}
	void set_semi(String semi){
		this.semi = semi;
	}

	int get_id() {
		return this.id;
	}
	String get_name() {
		return this.name;
	}
	String get_grade() {
		return this.grade;
	}
	String get_semi() {
		return this.semi;
	}

}
