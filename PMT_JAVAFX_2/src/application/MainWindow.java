package application;

import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainWindow {
	MainWindow(Sheet sheet, Stage pstage, PostgresLoginInfo logininfo) {

		// 情報を載せていくための大枠GridPaneたち
		MyPMTGUI sheetgui = new MyPMTGUI(sheet);
		//メニュー
		MyMenuBar menubar = new MyMenuBar(sheet, pstage, logininfo, sheetgui);

		//るーと
		VBox root = new VBox();

		// 各種設定
		root.getChildren().add(menubar.getMenuBar());
		root.getChildren().add(sheetgui.getGridPane());

		Scene scene = new Scene(root, 1440, 800);

		scene.getStylesheets().add(getClass().getResource("application.css").toString());

		pstage.setTitle("Project Management Tool 2");
		pstage.setScene(scene);
		pstage.show();
	}
}
