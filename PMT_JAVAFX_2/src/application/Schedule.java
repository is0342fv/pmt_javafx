package application;


import java.io.Serializable;

public class Schedule implements Serializable{

	int year = 2017;
	int month = 8;
	int date = 1;

	Schedule() {

	}

	void set_schedule(int year, int month, int date) {
		this.year = year;
		this.month = month;
		this.date = date;
	}
	int get_year() {
		return year;
	}
	int get_month() {
		return month;
	}
	int get_date() {
		return date;
	}
}
