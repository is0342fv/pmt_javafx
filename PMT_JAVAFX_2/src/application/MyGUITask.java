package application;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;

public class MyGUITask {

	GridPane taskpane = new GridPane();
	Task task;


	TextArea taskname = new TextArea("");
	TextArea personandplace = new TextArea("");

	List<Label> l_label = new ArrayList<Label>();
	List<Integer> l_status = new ArrayList<Integer>();

	TextArea perfection_tf = new TextArea("");
	TextArea devcomment = new TextArea("");
	TextArea tutorcomment = new TextArea("");

	MyGUITask(Task task_data) {

		task = task_data;
		set_taskString(task_data);

		perfection_tf.setMinHeight(10);

		// ＊＊＊＊taskpane＊＊＊＊
		GridPane left = new GridPane();
		GridPane center = new GridPane();
		GridPane right = new GridPane();
		taskpane.add(left, 0, 0);
		taskpane.add(center, 1, 0);
		taskpane.add(right, 2, 0);

		// taskpane 設定
		ColumnConstraints column1 = new ColumnConstraints();
		ColumnConstraints column2 = new ColumnConstraints();
		ColumnConstraints column3 = new ColumnConstraints();
		column1.setPrefWidth(1280/3);
		column2.setPrefWidth(1280/3);
		column3.setPrefWidth(1280/3); // magic number
		column1.setPercentWidth(100);
		column2.setPercentWidth(100);
		column3.setPercentWidth(100);
		taskpane.getColumnConstraints().addAll(column1, column2, column3);

		// -----------------

		// ＊＊＊＊left＊＊＊＊

		taskname.setWrapText(true);
		personandplace.setWrapText(true);
		left.add(taskname, 0, 0);
		left.add(personandplace, 1, 0);

		// left 設定
		column1 = new ColumnConstraints();
		column2 = new ColumnConstraints();
		column1.setPercentWidth(70);
		column2.setPercentWidth(30);
		left.getColumnConstraints().addAll(column1, column2);


		// ＊＊＊＊center＊＊＊＊


		// center 設定

		List <ColumnConstraints> l_column = new ArrayList<ColumnConstraints>();
//		List<Label> l_label = new ArrayList<Label>();
//		List<Integer> l_status = new ArrayList<Integer>();

		int i;
		for(i = 0; i<12; i++) {
			l_column.add(new ColumnConstraints());
			l_column.get(i).setPercentWidth(100);

			if(l_status.size() < 12) {
				for(int f=0; f<12; f++) {l_status.add(0);}
			}
			l_label.add(new Label());

			Label t_label = l_label.get(i);

			if(l_status.get(i) == 0) {
				t_label.setBackground(new Background(new BackgroundFill( Color.WHITE , new CornerRadii(0) , Insets.EMPTY )));
			} else if(l_status.get(i) == 1) {
				t_label.setBackground(new Background(new BackgroundFill( Color.GREEN , new CornerRadii(0) , Insets.EMPTY )));
			} else if(l_status.get(i) == 2) {
				t_label.setBackground(new Background(new BackgroundFill( Color.RED , new CornerRadii(0) , Insets.EMPTY )));
			} else if(l_status.get(i) == 3) {
				t_label.setBackground(new Background(new BackgroundFill( Color.YELLOW , new CornerRadii(0) , Insets.EMPTY )));
			}

			int t_i = i;


			t_label.setPrefSize(70, 100);

			// クリック時の動作
			t_label.setOnMouseClicked( (MouseEvent event) -> {
				switch(l_status.get(t_i)) {
				case 0:
					t_label.setBackground(new Background(new BackgroundFill( Color.GREEN , new CornerRadii(0) , Insets.EMPTY )));
					break;
				case 1:
					t_label.setBackground(new Background(new BackgroundFill( Color.RED , new CornerRadii(0) , Insets.EMPTY )));
					break;
				case 2:
					t_label.setBackground(new Background(new BackgroundFill( Color.YELLOW , new CornerRadii(0) , Insets.EMPTY )));
					break;
				case 3:
					t_label.setBackground(new Background(new BackgroundFill( Color.WHITE , new CornerRadii(0) , Insets.EMPTY )));
					break;
				}
				int num = l_status.get(t_i);
				l_status.set(t_i, (num + 1) % 4);
			});
			center.add(t_label, i, 0);
		}
		center.setPrefHeight(100);
		center.getColumnConstraints().addAll(l_column);

		// ＊＊＊＊right＊＊＊＊＊
		GridPane comments_left = new GridPane();
		GridPane comments_right = new GridPane();

		Label perfection_label = new Label("完成度（％）");
		Label devcomment_label = new Label("問題点\n備考");
		devcomment.setWrapText(true);
		tutorcomment.setWrapText(true);

		comments_left.add(perfection_label, 0, 0);
		comments_left.add(perfection_tf, 1, 0);
		comments_left.add(devcomment_label, 0, 1);
		comments_left.add(devcomment, 1, 1);
		comments_right.add(tutorcomment, 0, 0);
		right.add(comments_left, 0, 0);
		right.add(comments_right, 1, 0);

		// right 設定
		// 左側
		ColumnConstraints cc_0 = new ColumnConstraints();
		ColumnConstraints cc_1 = new ColumnConstraints();
		RowConstraints rc_0 = new RowConstraints();
		RowConstraints rc_1 = new RowConstraints();
		cc_0.setPercentWidth(40);
		cc_1.setPercentWidth(100);
		rc_0.setPercentHeight(30);
		rc_1.setPercentHeight(70);
		comments_left.getColumnConstraints().addAll(cc_0, cc_1);
		comments_left.getRowConstraints().addAll(rc_0, rc_1);
		// 全体設定（右側は必要なし）
		ColumnConstraints cc_0z = new ColumnConstraints();
		ColumnConstraints cc_1z = new ColumnConstraints();
		cc_0z.setPercentWidth(85);
		cc_1z.setPercentWidth(40);
		right.getColumnConstraints().addAll(cc_0z, cc_1z);


		taskpane.setPrefHeight(100);
		taskpane.setGridLinesVisible(true);
		left.setGridLinesVisible(true);
		center.setGridLinesVisible(true);
		right.setGridLinesVisible(true);
		comments_left.setGridLinesVisible(true);
		comments_right.setGridLinesVisible(true);

		taskpane.getStyleClass().addAll("taskpane", "taskpanetext");
		set_edited_event();

	}

	private void set_taskString(Task task_data) {
		checkNull(taskname, task_data.getTaskDetail().get_taskname());
		checkNull(personandplace, task_data.getTaskDetail().get_personandplace());
		checkNull(perfection_tf, ""+task_data.getPerfection().get_perfection());
		checkNull(devcomment, task_data.getDevComment().get_comment());
		checkNull(tutorcomment, task_data.getTutorComment().get_comment());

		l_status = task_data.getSchedule();

	}

	private void checkNull(TextArea ta, String str) {
		if(str == null) {
			ta.setText("");
		} else if(str.equals("null")) {
			ta.setText("");
		} else {
			ta.setText(str);
		}
	}

	void save_taskdata() {
		task.getTaskDetail().set_taskname(taskname.getText());
		task.getTaskDetail().set_personandplace(personandplace.getText());

		task.setSchedule(l_status);

		task.getPerfection().set_perfection(Integer.valueOf(perfection_tf.getText()));
		task.getDevComment().set_comment(devcomment.getText());
		task.getTutorComment().set_comment(tutorcomment.getText());
	}

	GridPane getPane() {
		return taskpane;
	}

	void set_edited_event() {

		taskname.setOnKeyPressed( (event) -> {
			taskname.lookup(".content").setStyle("-fx-background-color: #ffeaea;");
		});
		personandplace.setOnKeyTyped( (event) -> {
			personandplace.lookup(".content").setStyle("-fx-background-color: #ffeaea;");
		});
		perfection_tf.setOnKeyTyped( (event) -> {
			perfection_tf.lookup(".content").setStyle("-fx-background-color: #ffeaea;");
		});
		devcomment.setOnKeyTyped( (event) -> {
			devcomment.lookup(".content").setStyle("-fx-background-color: #ffeaea;");
		});
		tutorcomment.setOnKeyTyped( (event) -> {
			tutorcomment.lookup(".content").setStyle("-fx-background-color: #ffeaea;");
		});
	}

	void return_color() {
		taskname.lookup(".content").setStyle("-fx-background-color: white;");
		personandplace.lookup(".content").setStyle("-fx-background-color: white;");
		perfection_tf.lookup(".content").setStyle("-fx-background-color: white;");
		perfection_tf.lookup(".content").setStyle("-fx-background-color: white;");
		devcomment.lookup(".content").setStyle("-fx-background-color: white;");
		tutorcomment.lookup(".content").setStyle("-fx-background-color: white;");
	}


}
