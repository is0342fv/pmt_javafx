package application;

import java.util.List;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public class MyPMTGUI {
	GridPane sheet = new GridPane();

	Sheet sheet_data;
	MyGUIHead sheethead;
	MyGUIBody sheetbody;
	MyGUIFoot sheetfoot;

	MyPMTGUI(Sheet sheet_data) {

		/*
		 * Head部 Body部 Foot部に分けてつくる
		 */
		this.sheet_data = sheet_data;
		sheethead = new MyGUIHead(sheet_data);
		sheetbody = new MyGUIBody(sheet_data);
		sheetfoot = new MyGUIFoot(sheet_data, sheethead, sheetbody);

		// おおもとに追加
		sheet.add(sheethead.getPane(), 0, 0);
		sheet.add(sheetbody.getPane(), 0, 1);
		sheet.add(sheetfoot.getPane(), 0, 2);

		// 各種設定
		RowConstraints row1 = new RowConstraints();
		RowConstraints row2 = new RowConstraints();
		RowConstraints row3 = new RowConstraints();
		row1.setPercentHeight(25);
		row2.setPercentHeight(70);
		row3.setPercentHeight(5);
		sheet.getRowConstraints().addAll(row1, row2, row3);

		sheet.setPrefWidth(1280);
		sheet.setMinWidth(1380);
		sheet.setMaxWidth(2560);

		sheet.getStyleClass().addAll("sheet", "paneborder");

		sheet.setGridLinesVisible(true);


	}

	GridPane getGridPane() {
		return sheet;
	}

	MyGUIHead getMyGUIHead() {
		return sheethead;
	}
	MyGUIBody getMyGUIBody() {
		return sheetbody;
	}
	MyGUIFoot getMyGUIFoot() {
		return sheetfoot;
	}
	void writetoObject() {
		List<MyGUITask> l_guitask = sheetfoot.get_l_guitask();
		for(MyGUITask gui : l_guitask) {
			gui.save_taskdata();
			gui.return_color();
		}
		sheethead.save_headdata(sheet_data);
		sheethead.return_color();
	}

}
