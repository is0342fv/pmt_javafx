package application;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MyGUISchedule {
	GridPane schedulepane = new GridPane();
	Calendar cl = Calendar.getInstance();
	Integer year;
	Integer month;
	Integer date;
	Label month1;
	Label[] week = new Label[12];

	MyGUISchedule(Schedule schedule) {
		year = new Integer(schedule.get_year());
		month = new Integer(schedule.get_month());
		date = new Integer(schedule.get_date());

		show_schedule(year, month, date);

	}

	void show_schedule(Integer year, Integer month, Integer date) {
		Calendar cl = Calendar.getInstance();
		cl.set(year, month, date);

		month1 = new Label(month.toString());month = (month+1)%12;
		Label month2 = new Label(month.toString());month = (month+1)%12;
		Label month3 = new Label(month.toString());

		month1.setAlignment(Pos.CENTER);
		month2.setAlignment(Pos.CENTER);
		month3.setAlignment(Pos.CENTER);
		month1.setPrefWidth(300);
		month2.setPrefWidth(300);
		month3.setPrefWidth(300);


		for(int i=0; i<12; i++) {
			week[i] = new Label(date.toString());
			week[i].setAlignment(Pos.CENTER);
			week[i].setPrefWidth(100);
			cl.add(Calendar.DATE, 7);
			date = cl.get(Calendar.DATE);
		}

		GridPane schedulepane1 = new GridPane();
		GridPane schedulepane2 = new GridPane();
		schedulepane.add(schedulepane1, 0, 0);
		schedulepane.add(schedulepane2, 0, 1);

		schedulepane1.add(month1, 0, 0);
		schedulepane1.add(month2, 1, 0);
		schedulepane1.add(month3, 2, 0);

		for(int i=0; i<12; i++) {
			schedulepane2.add(week[i], i, 0);
		}


		// 行を等分割するように指定
		schedulepane.setPrefHeight(200);
		RowConstraints row1 = new RowConstraints();
		RowConstraints row2 = new RowConstraints();
		row1.setPrefHeight(200);
		row2.setPrefHeight(200);
		row1.setPercentHeight(100);
		row2.setPercentHeight(100);
		schedulepane.getRowConstraints().addAll(row1, row2);


		schedulepane1.setPrefHeight(200);
		schedulepane1.setPrefWidth(600);
		ColumnConstraints column1 = new ColumnConstraints();
		ColumnConstraints column2 = new ColumnConstraints();
		ColumnConstraints column3 = new ColumnConstraints();
		column1.setPercentWidth(100);
		column2.setPercentWidth(100);
		column3.setPercentWidth(100);
		row1 = new RowConstraints();
		row1.setPercentHeight(100);
		schedulepane1.getColumnConstraints().addAll(column1, column2, column3);
		schedulepane1.getRowConstraints().addAll(row1);


		schedulepane2.setPrefHeight(200);
		schedulepane2.setPrefWidth(600);
		List<ColumnConstraints> l_column = new ArrayList<ColumnConstraints>();
		for(int i=0; i<12; i++) {
			l_column.add(new ColumnConstraints());
			l_column.get(i).setPercentWidth(100);
		}
		row1 = new RowConstraints();
		row1.setPercentHeight(100);
		schedulepane2.getColumnConstraints().addAll(l_column);
		schedulepane2.getRowConstraints().addAll(row1);

		schedulepane.setGridLinesVisible(true);
		schedulepane1.setGridLinesVisible(true);
		schedulepane2.setGridLinesVisible(true);

		month1.setOnMouseClicked((event)-> {
			month1.setText(editlabel(month1));

			int month_i = Integer.parseInt(month1.getText());
			month2.setText(((month_i+1)%12) + "");
			month3.setText(((month_i+2)%12) + "");

			Integer date_num = Integer.parseInt(week[0].getText());
			cl.set(year, month_i, date_num);
			for(int i=0; i<12; i++) {
				week[i].setText(date_num.toString());
				week[i].setAlignment(Pos.CENTER);
				week[i].setPrefWidth(100);
				cl.add(Calendar.DATE, 7);
				date_num = cl.get(Calendar.DATE);
			}
		});
		week[0].setOnMouseClicked((event)-> {
			week[0].setText(editlabel(week[0]));

			int month_i = Integer.parseInt(month1.getText());
			month2.setText(((month_i+1)%12) + "");
			month3.setText(((month_i+2)%12) + "");

			Integer date_num = Integer.parseInt(week[0].getText());
			cl.set(year, month_i, date_num);
			for(int i=0; i<12; i++) {
				week[i].setText(date_num.toString());
				week[i].setAlignment(Pos.CENTER);
				week[i].setPrefWidth(100);
				cl.add(Calendar.DATE, 7);
				date_num = cl.get(Calendar.DATE);
			}
		});
	}

	int get_year() {
		return year;
	}
	int get_month() {
		return Integer.parseInt(month1.getText());
	}
	int get_date() {
		return Integer.parseInt(week[0].getText());
	}

	String editlabel(Label label) {
		// 新しいウインドウを生成
		Stage newStage = new Stage();
		// モーダルウインドウに設定
		newStage.initModality(Modality.APPLICATION_MODAL);
		// オーナーを設定
//		newStage.initOwner(primaryStage);

		// 新しいウインドウ内に配置するコンテンツを生成
		GridPane gp = new GridPane();
		gp.setAlignment(Pos.CENTER);
		gp.setHgap(10);
		gp.setVgap(10);
		gp.setPadding(new Insets(25, 25, 25, 25));

        Label l = LabelBuilder.create().text("編集画面")
                .prefWidth(800)
                .prefHeight(40)
                .alignment(Pos.CENTER)
                .build();
		gp.add(l, 0, 0, 2, 1);

		TextArea textarea = new TextArea(label.getText());
		textarea.setWrapText(true);
		gp.add(textarea, 0, 1, 2, 1);

		Button submit = new Button("決定");
		gp.add(submit, 0, 2);

		Button cancel = new Button("キャンセル");
		gp.add(cancel, 1, 2);

		submit.setOnAction((event) ->{
			label.setText(textarea.getText());
			newStage.close();
		});

		cancel.setOnAction((event) ->{
			newStage.close();
		});

		Scene scene = new Scene(gp, 300, 275);
		scene.getStylesheets().add(getClass().getResource("application.css").toString());
		gp.getStyleClass().addAll("editwindow");

		newStage.setScene(scene);
		// 新しいウインドウを表示
		newStage.showAndWait();

		label.setPrefWidth(800);
		label.setPrefHeight(300);
		label.setBackground( new Background( new BackgroundFill(Color.web("#ffeaea"), CornerRadii.EMPTY, Insets.EMPTY) ) );

		return label.getText();

	}

	GridPane getPane() {
		return schedulepane;
	}

	void return_color() {
		week[0].setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
		month1.setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
	}

}
