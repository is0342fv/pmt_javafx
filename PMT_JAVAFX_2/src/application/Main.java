package application;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public class Main extends Application implements Initializable {
	@Override
	public void start(Stage pstage) throws SQLException {

		// 各種データを保持するSheetオブジェクト
		Sheet sheet = new Sheet();
		sheet.setTestHead();
		sheet.insert_new_task();

		// postgresログイン情報
		PostgresLoginInfo logininfo = new PostgresLoginInfo();

		// ウィンドウ
		MainWindow mainwindow = new MainWindow(sheet, pstage, logininfo);

		// ログインフォーム
		LoginForm loginform = new LoginForm(pstage, logininfo);


	}
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
