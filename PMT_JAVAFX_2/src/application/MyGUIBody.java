package application;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.VBox;

public class MyGUIBody {
	ScrollPane body = new ScrollPane();
	VBox vb = new VBox();

	MyGUIBody(Sheet sheetdata) {
		body.setContent(vb);
		body.setHbarPolicy(ScrollBarPolicy.NEVER);
		body.setVbarPolicy(ScrollBarPolicy.NEVER);

	}


	VBox getVBox() {
		return vb;
	}

	ScrollPane getPane() {
		return body;
	}

}
