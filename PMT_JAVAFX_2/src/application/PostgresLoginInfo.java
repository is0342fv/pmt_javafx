package application;

public class PostgresLoginInfo {
	String id = "postgres";
	String pw = "password";

	PostgresLoginInfo() {

	}
	PostgresLoginInfo(String id, String pw) {
		this.id = id;
		this.pw = pw;
	}

	void set_id(String id) {
		this.id = id;
	}

	void set_pw(String pw) {
		this.pw = pw;
	}

	String get_id() {
		return id;
	}

	String get_pw() {
		return pw;
	}
}
