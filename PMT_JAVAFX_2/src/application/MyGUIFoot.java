package application;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.layout.GridPane;

public class MyGUIFoot {
	GridPane foot = new GridPane();
	Integer surface_index = new Integer(0);
	List<MyGUITask> l_guitask = new ArrayList<MyGUITask>();

	Sheet sheet_data;
	MyGUIHead sheethead;
	MyGUIBody sheetbody;

	MyGUIFoot(Sheet sheet_data, MyGUIHead sheethead, MyGUIBody sheetbody) {
		this.sheet_data = sheet_data;
		this.sheethead = sheethead;

		// すでにデータが存在している場合、とりあえず全部表示
		if(sheet_data.get_tasklist().size() > 0) {
			for(int i=0; i<sheet_data.get_tasklist().size(); i++) {
				Label l_num = LabelBuilder.create().text((surface_index+1)+"")
						.prefWidth(30)
						.prefHeight(15)
						.alignment(Pos.CENTER)
						.build();
				surface_index++;
				MyGUITask taskpane = new MyGUITask(sheet_data.get_tasklist().get(i));
				l_guitask.add(taskpane);
				sheetbody.getVBox().getChildren().add(l_num);
				sheetbody.getVBox().getChildren().add(taskpane.getPane());
			}
		}


		// 追加ボタンで1列追加
		Button btn1 = new Button("追加");
		foot.add(btn1, 0, 0);

		btn1.setOnMouseClicked((event) -> {
			sheet_data.insert_new_task();
			Label l_num = LabelBuilder.create().text((surface_index+1)+"")
	                .prefWidth(30)
	                .prefHeight(15)
	                .alignment(Pos.CENTER)
	                .build();
			MyGUITask taskpane = new MyGUITask(sheet_data.get_tasklist().get(surface_index));
			l_guitask.add(taskpane);
			sheetbody.getVBox().getChildren().add(l_num);
			sheetbody.getVBox().getChildren().add(taskpane.getPane());
			surface_index++;
		});


		// 緊急措置
		/* MenuBarに統合
		Button btn2 = new Button("確定");
		foot.add(btn2, 1, 0);

		btn2.setOnMouseClicked((event) -> {
			for(MyGUITask gui : l_guitask) {
				gui.save_taskdata();
				gui.return_color();
			}
			sheethead.save_headdata(sheet_data);
			sheethead.return_color();
		})
		*/;
	}

	List<MyGUITask> get_l_guitask() {
		return l_guitask;
	}

	GridPane getPane() {
		return foot;
	}

}
