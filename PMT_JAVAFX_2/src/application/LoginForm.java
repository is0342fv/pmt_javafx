package application;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginForm {

	LoginForm(Stage primaryStage, PostgresLoginInfo logininfo) {
		System.out.println("処理開始！");
		// 新しいウインドウを生成
		Stage newStage = new Stage();
		// モーダルウインドウに設定
		newStage.initModality(Modality.APPLICATION_MODAL);
		// オーナーを設定
		newStage.initOwner(primaryStage);

		// 新しいウインドウ内に配置するコンテンツを生成
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Text scenetitle = new Text("Welcome");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);

		Text information = new Text("");
		grid.add(information, 0, 1, 2, 1);

		Label userName = new Label("User Name:");
		grid.add(userName, 0, 2);

		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 2);

		Label pw = new Label("Password:");
		grid.add(pw, 0, 3);

		TextField pwBox = new TextField();
		grid.add(pwBox, 1, 3);

		Button btn = new Button("Sign in");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 5);

		final Text actiontarget = new Text();
		grid.add(actiontarget, 1, 7);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				actiontarget.setFill(Color.FIREBRICK);
				try {
					if(logintopostgres()) {
						actiontarget.setText("Login Successed\n");
						logininfo.set_id(userTextField.getText());
						logininfo.set_pw(pwBox.getText());

						Alert dialog = new Alert(AlertType.INFORMATION);
						dialog.setHeaderText(null);
						dialog.setContentText("ログイン成功");
						dialog.showAndWait();

						newStage.close();
					} else {
						actiontarget.setText("Login Failed\nこの状態ではユーザpostgresが使われます");
					}
				} catch (SQLException e1) {
					// TODO 自動生成された catch ブロック
					e1.printStackTrace();
				}

			}

			private boolean logintopostgres() throws SQLException {
				boolean result = false;
				result = new JDBC().login(userTextField.getText(), pwBox.getText());
				return result;
			}
		});

		newStage.setScene(new Scene(grid, 300, 275));
		// 新しいウインドウを表示
		newStage.show();
	}

}
