package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JDBC {

	Sheet sheet = null;
	String id = "postgres";
	String pw = "password";

	JDBC() {
		// logintest用
	}

    JDBC(Sheet sheet, PostgresLoginInfo logininfo) throws SQLException {
    	this.sheet = sheet;
    	id = logininfo.get_id();
    	pw = logininfo.get_pw();
    }

    void saveToSheet() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            //-----------------
            // 接続
            //-----------------
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/pmtdatabase", // "jdbc:postgresql://[場所(Domain)]:[ポート番号]/[DB名]"
                    id, // ログインロール
                    pw); // パスワード
            statement = connection.createStatement();

            //-----------------
            // SQLの発行
            //-----------------
            //ユーザー情報のテーブル
            Calendar c = Calendar.getInstance();
//　ロード時のテーブル指定がめんどうなので後回し
//    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MMdd_HHmm_ss");
//            String tablename = "datatest_" + sdf.format(c.getTime());
            String tablename = "datatest";
            String tablename_head = tablename + "_head";

//			テーブルがなければ作るべきだしあれば作らないべき
           	createTable(statement, tablename);
           	createTable_head(statement, tablename_head);

            statement.execute("DELETE FROM " + tablename);
            statement.execute("DELETE FROM " + tablename_head);

            executeSQLFromSheet(statement, tablename, tablename_head);

            showResult(statement, tablename);
            showResult(statement, tablename_head);

        } finally {
            //接続を切断する
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    Sheet loadFromSheet() throws SQLException {
    	Sheet sheet = new Sheet();
    	sheet.setTestHead();

    	// シートの読み込み
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            //-----------------
            // 接続
            //-----------------
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/pmtdatabase", // "jdbc:postgresql://[場所(Domain)]:[ポート番号]/[DB名]"
                    id, // ログインロール
                    pw); // パスワード
            statement = connection.createStatement();

            //-----------------
            // SQLの発行
            //-----------------
            //ユーザー情報のテーブル
            String tablename = "datatest";
            String tablename_head = tablename + "_head";

            if(checkTableExist(statement, tablename) && checkTableExist(statement, tablename_head)) {
                writeToSheet(statement, tablename, tablename_head, sheet);
            } else {
            	sheet = null;
            }


        } finally {
            //接続を切断する
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }


    	return sheet;
    }

	void createTable(Statement statement, String tablename) throws SQLException {

        if(!checkTableExist(statement, tablename)) {

        	statement.execute(
        		"CREATE TABLE "+ tablename +" (\r\n" +
        		"    taskname            TEXT,\r\n" +
        		"    personandplace      TEXT,\r\n" +
        		"    schedule            TEXT,\r\n" +
        		"    perfection          INTEGER,\r\n" +
        		"    developercomment    TEXT,\r\n" +
        		"    tutorcommentCHAR    TEXT\r\n" +
        		");" +
        		"\r\n"
        		);
        	System.out.println(tablename + " テーブルが存在しなかったため作成しました．");

        } else {

        }

    }

	void createTable_head(Statement statement, String tablename) throws SQLException {

        if(!checkTableExist(statement, tablename)) {

        	statement.execute(
        			"CREATE TABLE "+ tablename +" (\r\n" +
        					"    id                INTEGER,\r\n" +
        					"    name              TEXT,\r\n" +
        					"    grade             TEXT,\r\n" +
        					"    semi              TEXT,\r\n" +
        					"    plan              TEXT,\r\n" +
        					"    filled            TEXT\r\n" +
        					");" +
        					"\r\n"
        			);
        	System.out.println(tablename + " テーブルが存在しなかったため作成しました．");

        } else {

        }

    }


	boolean checkTableExist(Statement statement, String tablename) throws SQLException {
		ResultSet resultSet = statement.executeQuery("select \r\n" +
				"	relname as TABLE_NAME \r\n" +
				"from \r\n" +
				"	pg_stat_user_tables");
		boolean alreadyexist = false;

		List<String> fields = new ArrayList<String>();
        ResultSetMetaData rsmd = resultSet.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            fields.add(rsmd.getColumnName(i));
        }

        //結果の出力
        System.out.println("テーブル一覧");
        while (resultSet.next()) {
            //値は、「resultSet.getString(<フィールド名>)」で取得する。
            for (String field : fields) {
                System.out.println(field + ":" + resultSet.getString(field));
                if(resultSet.getString(field).equals(tablename)) alreadyexist = true;
            }
        }

        if(resultSet != null) {
        	resultSet.close();
        }
        return alreadyexist;
	}

    ResultSet showResult(Statement statement, String tablename) throws SQLException {
    	ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tablename);

        //-----------------
        // 値の取得
        //-----------------
        // フィールド一覧を取得
        List<String> fields = new ArrayList<String>();
        ResultSetMetaData rsmd = resultSet.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            fields.add(rsmd.getColumnName(i));
        }

        //結果の出力
        int rowCount = 0;
        while (resultSet.next()) {
            rowCount++;

            System.out.println("---------------------------------------------------");
            System.out.println("--- Rows:" + rowCount);
            System.out.println("---------------------------------------------------");

            //値は、「resultSet.getString(<フィールド名>)」で取得する。
            for (String field : fields) {
                System.out.println(field + ":" + resultSet.getString(field));
            }
        }

        return resultSet;
    }

    void executeSQLFromSheet(Statement statement, String tablename, String tablename_head) throws SQLException {
    	// head
    	statement.execute(
        		"INSERT INTO "+ tablename_head +" VALUES (\r\n" +
        		"	'" + sheet.get_author().get_id() + "',\r\n" +
        		"	'" + sheet.get_author().get_name() + "',\r\n" +
        		"	'" + sheet.get_author().get_grade() + "',\r\n" +
        		"	'" + sheet.get_author().get_semi() + "',\r\n" +
        		"	'" + sheet.get_plan() + "',\r\n" +
        		"	'" + sheet.get_strfilled() + "'\r\n" +
        		")"
        		);

    	// body
    	List<Task> tasklist = sheet.get_tasklist();

		for(Task task : tasklist) {
			insertData(statement, tablename, task);
		}
    }

    void insertData(Statement statement, String tablename, Task task) throws SQLException {

    	statement.execute(
        		"INSERT INTO "+ tablename +" VALUES (\r\n" +
        		"	'" + task.getTaskDetail().get_taskname() + "',\r\n" +
        		"	'" + task.getTaskDetail().get_personandplace() + "',\r\n" +
        		"	'" + progresstoString(task.getSchedule()) + "',\r\n" +
        		"	'" + task.getPerfection().get_perfection() + "',\r\n" +
        		"	'" + task.getDevComment().get_comment() + "',\r\n" +
        		"	'" + task.getTutorComment().get_comment() + "'\r\n" +
        		")"
        		);
    }

    private String progresstoString(List<Integer> l_status) {
    	StringBuilder result = new StringBuilder("");

    	for(Integer status : l_status) {
    		result.append(status.toString());
    	}

		return result.toString();

    }


// ロード時
    void writeToSheet(Statement statement, String tablename, String tablename_head, Sheet sheet) throws SQLException {

    	// head
       	ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tablename_head);
        // フィールド一覧を取得
        List<String> fields = new ArrayList<String>();
        ResultSetMetaData rsmd = resultSet.getMetaData();


        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            fields.add(rsmd.getColumnName(i));
        }
        //書き込み
        while (resultSet.next()) {
        	Person author = new Person();
        	author.set_id(resultSet.getInt(fields.get(0)));
        	author.set_name(resultSet.getString(fields.get(1)));
        	author.set_grade(resultSet.getString(fields.get(2)));
        	author.set_semi(resultSet.getString(fields.get(3)));
        	sheet.set_author(author);
        	sheet.set_plan(resultSet.getString(fields.get(4)));
        	sheet.set_filled_inStr(resultSet.getString(fields.get(5)));
        }
        resultSet.close();

    	// body
       	resultSet = statement.executeQuery("SELECT * FROM " + tablename);
       	Task task;
        //-----------------
        // 値の取得
        //-----------------
        // フィールド一覧を取得
        fields = new ArrayList<String>();
        rsmd = resultSet.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            fields.add(rsmd.getColumnName(i));
        }

        //書き込み
        while (resultSet.next()) {

            task = sheet.insert_new_task();
            task.setTaskDetails(resultSet.getString(fields.get(0)), resultSet.getString(fields.get(1)));
            task.setScheduleFromString(resultSet.getString(fields.get(2)));
            task.setPerfection(resultSet.getInt(fields.get(3)));
            task.setDevComment(resultSet.getString(fields.get(4)));
            task.setTutorComment(resultSet.getString(fields.get(5)));

        }



	}

    boolean login(String id, String pw) {
    	boolean result = false;

    	Connection connection = null;
    	Statement statement = null;
    	ResultSet resultSet = null;

    	try {
    		System.out.println(id +", "+ pw +"でログインを試みます");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/pmtdatabase", // "jdbc:postgresql://[場所(Domain)]:[ポート番号]/[DB名]"
					id, // ログインロール
					pw);
			connection.close();
		} catch (SQLException e) {
			System.out.println("ログインできませんでした");
			return false;
		}

		return true;

    }

}

