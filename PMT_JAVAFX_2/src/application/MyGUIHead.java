package application;

import java.text.SimpleDateFormat;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
public class MyGUIHead {
	GridPane head = new GridPane();
	Sheet sheet_data;
	MyGUISchedule schedulepane;
	Label lb2;
	Label lb7;
	Label lb9;
	Label lb11;

	MyGUIHead(Sheet sheet_data) {

		this.sheet_data = sheet_data;
		schedulepane = new MyGUISchedule(sheet_data.get_schedule());
		// **** ごっちゃごちゃゾーン ****

		// １－３列目１行目
		GridPane gp1 = new GridPane();
		Label lb1 		= new Label("開発・実験する項目");
		lb2 		= new Label(sheet_data.get_author().get_name()); // temp
		Label lb3 		= new Label("フォロー記録");
		gp1.add(lb1, 0, 0);
		gp1.add(lb2, 1, 0);
		gp1.add(lb3, 2, 0);

		// １列目２－３行目
			GridPane gp2 = new GridPane();
			Label lb4 		= new Label("実施内容");
			Label lb5 		= new Label("実施者（実施場所）");
			gp2.add(lb4, 0, 0);
			gp2.add(lb5, 1, 0);
		gp1.add(gp2, 0, 1);

		// ２－３列目２行目
		GridPane gp3 = new GridPane();
			Label lb6 		= new Label("実施時期（日付の日までに実施すること）");
			gp3.add(lb6, 0, 0);
			gp3.add(schedulepane.getPane(), 0, 1);
		gp1.add(gp3, 1, 1);

		// ２－３列目３行目
			GridPane gp4 = new GridPane();
			lb7 		= new Label("フォロー記録は、実施日ごとに1シートをコピーして作成すること");
			if (sheet_data.get_attention() != null) {
				lb7.setText(sheet_data.get_attention());
			}
			gp4.add(lb7, 0, 0);
				GridPane gp5 = new GridPane();
				Label lb8 		= new Label("予定");
				lb9 		= new Label("１週間に１回，フォロー");
				if(sheet_data.get_plan() != null) lb9.setText(sheet_data.get_plan());
				Label lb10 		= new Label("実施日");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 E曜日");
				lb11		= new Label(sdf.format(sheet_data.get_filleddate().getTime())); // temp
				if(sheet_data.get_strfilled() != null) lb11.setText(sheet_data.get_strfilled());
				Label lb12 		= new Label("チュータ意見");
				gp5.add(lb8, 0, 0);
				gp5.add(lb9, 1, 0);
				gp5.add(lb10, 0, 1);
					GridPane gp6 = new GridPane();
					gp6.add(lb11, 0, 0);
					gp6.add(lb12, 1, 0);
				gp5.add(gp6, 1, 1);
			gp4.add(gp5, 0, 1);
		gp1.add(gp4, 2, 1);


		// 一番大きな枠：行を３分割するように指定
		ColumnConstraints column1 = new ColumnConstraints();
		ColumnConstraints column2 = new ColumnConstraints();
		ColumnConstraints column3 = new ColumnConstraints();
		column1.setPrefWidth(1280/3);
		column2.setPrefWidth(1280/3);
		column3.setPrefWidth(1280/3); // magic number
		column1.setPercentWidth(100);
		column2.setPercentWidth(100);
		column3.setPercentWidth(100);
		gp1.getColumnConstraints().addAll(column1, column2, column3);

		// 一番大きな枠：列の大きさを設定
		RowConstraints row1 = new RowConstraints();
		RowConstraints row2 = new RowConstraints();
		row1.setPrefHeight(40);
		row2.setPrefHeight(160);
		row1.setPercentHeight(40);
		row2.setPercentHeight(160);
		gp1.getRowConstraints().addAll(row1, row2);

		// 実施内容・実施者のところ
		column1 = new ColumnConstraints();
		column2 = new ColumnConstraints();
		column1.setPercentWidth(70);
		column2.setPercentWidth(30);
		gp2.getColumnConstraints().addAll(column1, column2);

		row1 = new RowConstraints();
		row1.setPercentHeight(100);
		gp2.getRowConstraints().add(row1);

		// 実施時期・スケジュールのところ
		column1 = new ColumnConstraints();
		column1.setPercentWidth(100);
		gp3.getColumnConstraints().add(column1);

		row1 = new RowConstraints();
		row2 = new RowConstraints();
		row1.setPercentHeight(50);
		row2.setPercentHeight(50);
		gp3.getRowConstraints().addAll(row1, row2);

		// フォロー記録・予定実施日のところ
		column1 = new ColumnConstraints();
		column1.setPercentWidth(100);
		gp4.getColumnConstraints().add(column1);

		row1 = new RowConstraints();
		row2 = new RowConstraints();
		row1.setPercentHeight(50);
		row2.setPercentHeight(50);
		gp4.getRowConstraints().addAll(row1, row2);

		// 予定実施日・本文と意見のところ
		column1 = new ColumnConstraints();
		column2 = new ColumnConstraints();
		column1.setPercentWidth(20);
		column2.setPercentWidth(80);
		gp5.getColumnConstraints().addAll(column1, column2);

		row1 = new RowConstraints();
		row2 = new RowConstraints();
		row1.setPercentHeight(50);
		row2.setPercentHeight(50);
		gp5.getRowConstraints().addAll(row1, row2);

		// 日付・チュータ意見のところ
		column1 = new ColumnConstraints();
		column2 = new ColumnConstraints();
		column1.setPercentWidth(60);
		column2.setPercentWidth(40);
		gp6.getColumnConstraints().addAll(column1, column2);

		row1 = new RowConstraints();
		row1.setPercentHeight(100);
		gp6.getRowConstraints().add(row1);

		// 枠線をつけます
		gp1.setGridLinesVisible(true);
		gp2.setGridLinesVisible(true);
		gp3.setGridLinesVisible(true);
		gp4.setGridLinesVisible(true);
		gp5.setGridLinesVisible(true);
		gp6.setGridLinesVisible(true);

		gp1.getStyleClass().addAll("pane","paneborder");
		gp2.getStyleClass().addAll("pane","paneborder");
		gp3.getStyleClass().addAll("pane","paneborder");
		gp4.getStyleClass().addAll("pane","paneborder");
		gp5.getStyleClass().addAll("pane","paneborder");
		gp6.getStyleClass().addAll("pane","paneborder");

		gp1.setPrefHeight(400);

		head.add(gp1, 0, 0);


		lb2.setOnMouseClicked((event)-> {
			lb2.setText(editlabel(lb2));
		});
		lb7.setOnMouseClicked((event)-> {
			lb7.setText(editlabel(lb7));
		});
		lb9.setOnMouseClicked((event)-> {
			lb9.setText(editlabel(lb9));
		});
		lb11.setOnMouseClicked((event)-> {
			lb11.setText(editlabel(lb11));
		});
	}

	String getAuthor() {
		return lb2.getText();
	}
	String getAttention() {
		return lb7.getText();
	}
	String getPlan() {
		return lb9.getText();
	}
	String getFilled() {
		return lb11.getText();
	}

	void save_headdata(Sheet sheetdata) {
		sheetdata.get_author().set_name(getAuthor());
		sheetdata.set_attention(getAttention());
		sheetdata.set_filled_inStr(getFilled());
		sheetdata.set_plan(getPlan());
		sheetdata.get_schedule().set_schedule(schedulepane.get_year(), schedulepane.get_month(), schedulepane.get_date());
	}

	GridPane getPane() {
		return head;
	}

	String editlabel(Label label) {
		// 新しいウインドウを生成
		Stage newStage = new Stage();
		// モーダルウインドウに設定
		newStage.initModality(Modality.APPLICATION_MODAL);
		// オーナーを設定
//		newStage.initOwner(primaryStage);

		// 新しいウインドウ内に配置するコンテンツを生成
		GridPane gp = new GridPane();
		gp.setAlignment(Pos.CENTER);
		gp.setHgap(10);
		gp.setVgap(10);
		gp.setPadding(new Insets(25, 25, 25, 25));

        Label l = LabelBuilder.create().text("編集画面")
                .prefWidth(800)
                .prefHeight(40)
                .alignment(Pos.CENTER)
                .build();
		gp.add(l, 0, 0, 2, 1);

		TextArea textarea = new TextArea(label.getText());
		textarea.setWrapText(true);
		gp.add(textarea, 0, 1, 2, 1);

		Button submit = new Button("決定");
		gp.add(submit, 0, 2);

		Button cancel = new Button("キャンセル");
		gp.add(cancel, 1, 2);

		submit.setOnAction((event) ->{
			label.setText(textarea.getText());
			newStage.close();
		});

		cancel.setOnAction((event) ->{
			newStage.close();
		});

		Scene scene = new Scene(gp, 300, 275);
		scene.getStylesheets().add(getClass().getResource("application.css").toString());
		gp.getStyleClass().addAll("editwindow");

		newStage.setScene(scene);
		// 新しいウインドウを表示
		newStage.show();

		label.setPrefWidth(800);
		label.setPrefHeight(300);
		label.setBackground( new Background( new BackgroundFill(Color.web("#ffeaea"), CornerRadii.EMPTY, Insets.EMPTY) ) );

		return label.getText();

	}

	void return_color() {
		lb2.setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
		lb7.setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
		lb9.setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
		lb11.setBackground( new Background( new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY) ) );
		schedulepane.return_color();
	}

}
