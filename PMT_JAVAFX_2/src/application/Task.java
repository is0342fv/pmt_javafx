package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Task implements Serializable{

	List<Integer> schedule = new ArrayList<Integer>(); // あとまわし♡
	TaskDetail taskdetail = new TaskDetail();
	Progress progress = new Progress();
	Perfection perfection = new Perfection();
	DeveloperComment devcomment = new DeveloperComment();
	TutorComment tutorcomment = new TutorComment();

	public Task() {
	}

	/*
	 * method
	 */
	void setTaskDetails(String taskname, String personandplace) {
		taskdetail.set_taskname(taskname);
		taskdetail.set_personandplace(personandplace);
	}
	void setProgress() {
	}
	void setPerfection(int num) {
		perfection.set_perfection(num);
	}
	void setDevComment(String comment) {
		devcomment.set_comment(comment);
	}
	void setTutorComment(String comment) {
		tutorcomment.set_comment(comment);
	}
	void setSchedule(List<Integer> l_status) {
		this.schedule = l_status;
	}
	void setScheduleFromString(String text) {
		List<Integer> l_status = new ArrayList<Integer>();

		String[] strArray = text.split("");
		for(String s : strArray) {
			l_status.add(Integer.parseInt(s));
		}


		this.schedule = l_status;
	}

	TaskDetail getTaskDetail() {
		return taskdetail;
	}
	Progress getProgress() {
		return progress;
	}
	Perfection getPerfection() {
		return perfection;
	}
	DeveloperComment getDevComment() {
		return devcomment;
	}
	TutorComment getTutorComment() {
		return tutorcomment;
	}
	List<Integer> getSchedule() {
		return schedule;
	}

}
