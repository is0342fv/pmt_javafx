package application;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Sheet implements Serializable {
	Person author = new Person();
	Calendar filled = null;
	List<Task> tasklist = new ArrayList<Task>();
	Schedule schedule = new Schedule(); //スケジュールについてのデータ
	String attention = null;
	String plan = null;
	String s_filled = null;
//	List<Integer> l_status = new ArrayList<Integer>();

	void set_author(Person author) {
		this.author = author;
	}

	Person get_author() {
		return author;
	}

	void set_filled(Calendar filled) {
		this.filled = filled;
	}
	void set_filled_inStr(String filled) {
		this.s_filled = filled;
	}
	void set_plan(String plan) {
		this.plan = plan;
	}

	Calendar get_filleddate() {
		return filled;
	}

	Schedule get_schedule() {
		return schedule;
	}


	Task insert_new_task() {
		/*
		 * タスクをひとつ追加（挿入）する
		 */
		Task task = new Task();
		tasklist.add(task);
		return task;
	}

	List<Task> get_tasklist() {
		return this.tasklist;
	}

	List<Task> clear_tasklist() {
		tasklist = new ArrayList<Task>();
		return tasklist;
	}

	public void setTestHead() {
		author.set_name("テスト姓 テスト名");
		author.set_id(000);
		author.set_grade("B0");
		author.set_semi("test semi");
		schedule.set_schedule(2019, 8, 7);
		set_author(author);

		Calendar cl = Calendar.getInstance();
		cl.set(2000, 1, 1);

		set_filled(cl);
	}

	public void setTestCase() {

		Task task = insert_new_task();

		task.setPerfection(0);
		task.setTaskDetails("テスト実験", author.get_name() + "\n" + "C000");
		task.setDevComment("テスト開発者コメント");
		task.setTutorComment("テストチュータコメント");
		task.setProgress();

	}

	String get_plan() {
		return plan;
	}

	String get_strfilled() {
		return s_filled;
	}

	public void set_attention(String attention) {
		this.attention = attention;

	}

	public String get_attention() {
		return this.attention;
	}

}
